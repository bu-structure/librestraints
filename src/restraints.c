#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include "restraints.h"
#include "khash.h"

#include <assert.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <jansson.h>

// Parse a group. Returns -1 on error, or number of restraints in group on success.
static int parse_restraint_group(struct restraint_group *group, json_t *obj);
// Parses a restraint.
static bool parse_restraint(struct restraint *restraint, json_t *obj);

bool restraint_set_read_json(struct restraint_set *rs, const char *restraints_path)
{
    FILE *fp = fopen(restraints_path, "r");
    if (fp == NULL)
        return false;

    return restraint_set_fread_json(rs, fp);
}

bool restraint_set_fread_json(struct restraint_set *rs, FILE *fp)
{
    bool has_error = false;
    json_error_t json_error;
    json_t *root = json_loadf(fp, 0, &json_error);
    json_t *tmp;

    // Basic json integrity checks
    if (!root) {
        fprintf(stderr, "error loading restraints: on line %d: %s\n",
                json_error.line, json_error.text);
        goto set_error;
    }

    if (!json_is_object(root)) {
        fprintf(stderr, "error: json input expect to be a json object\n");
        goto set_error;
    }

    json_t *groups = json_object_get(root, "groups");
    if (!groups || !json_is_array(groups)) {
        fprintf(stderr, "error: groups not an array\n");
        goto set_error;
    }

    // Passed basic integrity checks, setup rs
    rs->ngroups = json_array_size(groups);
    rs->required = rs->ngroups; // Default is all required
    rs->groups =
        calloc(rs->ngroups, sizeof(struct restraint_group));

    tmp = json_object_get(root, "set_limit");
    if (tmp != NULL) {
        rs->required = rs->ngroups - json_integer_value(tmp);
    }
    tmp = json_object_get(root, "required");
    if (tmp != NULL) {
        rs->required = json_integer_value(tmp);
    }

    rs->nrestraints_total = 0;

    struct restraint_group *group;
    int group_size;
    size_t i;
    json_t *obj;

    json_array_foreach(groups, i, obj) {
        if (!obj || !json_is_object(obj)) {
            fprintf(stderr, "group not an object\n");
            goto set_error;
        }
        group = &rs->groups[i];

        group_size = parse_restraint_group(group, obj);
        if (group_size < 0) {
            goto set_error;
        }
        rs->nrestraints_total += group_size;
    }

    // Collect all the restraints
    rs->all_restraints =
        malloc(rs->nrestraints_total * sizeof(struct restraint *));
    int restraint_i = 0;
    for (i = 0; i < rs->ngroups; ++i)
    {
        group = &rs->groups[i];
        for (size_t j = 0; j < group->nrestraints; ++j)
        {
            rs->all_restraints[restraint_i] = &group->restraints[j];
            ++restraint_i;
        }
    }

    goto cleanup; // skip has_error

set_error:
    has_error = true;
cleanup:
    if (root)
        json_decref(root);
    if (fp)
        fclose(fp);
    if (has_error)
        restraint_set_destroy(rs);
    return !has_error;
}

static int parse_restraint_group(struct restraint_group *group, json_t *obj)
{
    json_t *restraints = json_object_get(obj, "restraints");

    group->nrestraints = json_array_size(restraints);
    group->restraints = calloc(group->nrestraints, sizeof(struct restraint));
    group->required = group->nrestraints;
    group->group_required = false;

    json_t *tmp;

    tmp = json_object_get(obj, "required");
    if (tmp != NULL) {
        group->required = json_integer_value(tmp);
    }

    tmp = json_object_get(obj, "group_required");
    if (tmp != NULL) {
        group->group_required = json_boolean_value(tmp);
    }

    struct restraint *r;
    size_t index;

    json_array_foreach(restraints, index, tmp) {
        r = &group->restraints[index];

        if (!json_is_object(tmp)) {
            fprintf(stderr, "restraint not an object\n");
            return -1;
        }

        if (!parse_restraint(r, tmp)) {
            return -1;
        }
    }

    return group->nrestraints;
}

static bool parse_restraint(struct restraint *restraint, json_t *obj)
{
    json_t *tmp;
    json_error_t error;

    if(json_unpack_ex(obj, &error, 0, "{s:F, s:F, s?F}",
                      "dmin", &restraint->dmin,
                      "dmax", &restraint->dmax,
                      "dist", &restraint->dist) == -1) {
        fprintf(stderr, "Error parsing distances: %s\n", error.text);
        return false;
    }
    restraint->dmin_sq = pow(restraint->dmin, 2.0);
    restraint->dmax_sq = pow(restraint->dmax, 2.0);
    restraint->rec_resi_id.insertion = restraint->lig_resi_id.insertion = ' ';

    restraint->rec_type = R_RESIDUE;
    tmp = json_object_get(obj, "rec_type");
    if (tmp) {
        if (!json_is_string(tmp)) {
            fprintf(stderr, "rec_type not string\n");
            return false;
        }
        const char *type = json_string_value(tmp);
        if (strcmp(type, "residue") == 0) {
            restraint->rec_type = R_RESIDUE;
        } else if (strcmp(type, "chain") == 0) {
            restraint->rec_type = R_CHAIN;
        } else if (strcmp(type, "all") == 0) {
            restraint->rec_type = R_AG;
        } else if (strcmp(type, "range") == 0) {
            restraint->rec_type = R_RANGE;
        } else if (strcmp(type, "atom") == 0) {
            restraint->rec_type = R_ATOM;
        }
    }

    restraint->lig_type = R_RESIDUE;
    tmp = json_object_get(obj, "lig_type");
    if (tmp) {
        if (!json_is_string(tmp)) {
            fprintf(stderr, "lig_type not string\n");
            return false;
        }
        const char *type = json_string_value(tmp);
        if (strcmp(type, "residue") == 0) {
            restraint->lig_type = R_RESIDUE;
        } else if (strcmp(type, "chain") == 0) {
            restraint->lig_type = R_CHAIN;
        } else if (strcmp(type, "all") == 0) {
            restraint->lig_type = R_AG;
        } else if (strcmp(type, "range") == 0) {
            restraint->lig_type = R_RANGE;
        } else if (strcmp(type, "atom") == 0) {
            restraint->lig_type = R_ATOM;
        }
    }

    const char *resind;
    const char *chain;
    json_t *atom;
    switch (restraint->rec_type) {
        case R_RESIDUE:
            tmp = json_object_get(obj, "rec_chain");
            if (!tmp) {
                fprintf(stderr, "rec_chain for specifying residue chain must be present\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "rec_chain must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->rec_resi_id.chain = chain[0];

            tmp = json_object_get(obj, "rec_resid");
            if (!tmp) {
                tmp = json_object_get(obj, "rec_resi");
            }
            if (!tmp) {
                fprintf(stderr, "rec_resi key for specifying residue not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->rec_resi_id.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->rec_resi_id.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->rec_resi_id.residue_seq = ivalue;
            } else {
                fprintf(stderr, "rec_resi must be an integer or string\n");
                return false;
            }
            break;
        case R_CHAIN:
            tmp = json_object_get(obj, "rec_chain");
            if (!tmp) {
                fprintf(stderr, "rec_chain for specifying residue chain not present\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "rec_chain not string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->rec_chain = chain[0];
            break;
        case R_AG:
            break;
        case R_RANGE:
            atom = json_object_get(obj, "rec_atom_start");
            if (!atom) {
                fprintf(stderr, "rec_atom_start for specifying start of range must be present\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "rec_atom_start must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in rec_atom_start must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->rec_range.atom_start.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for rec_atom_start not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->rec_range.atom_start.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->rec_range.atom_start.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->rec_range.atom_start.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in rec_atom_start must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "rec_atom_start must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in rec_atom_start must be a string\n");
                return false;
            }
            strncpy(restraint->rec_range.atom_start.atom_name, json_string_value(tmp), 4);

            atom = json_object_get(obj, "rec_atom_end");
            if (!atom) {
                fprintf(stderr, "rec_atom_end for specifying end of range must be present\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "rec_atom_end must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in rec_atom_end must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->rec_range.atom_end.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for rec_atom_end not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->rec_range.atom_end.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->rec_range.atom_end.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->rec_range.atom_end.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in rec_atom_end must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "rec_atom_end must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in rec_atom_end must be a string\n");
                return false;
            }
            strncpy(restraint->rec_range.atom_end.atom_name, json_string_value(tmp), 4);
            break;
        case R_ATOM:
            atom = json_object_get(obj, "rec_atom");
            if (!atom) {
                fprintf(stderr, "rec_atom must be present for atom type restraints\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "rec_atom must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in rec_atom must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->rec_atom_id.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for rec_atom not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->rec_atom_id.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->rec_atom_id.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->rec_atom_id.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in rec_atom must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "rec_atom must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in rec_atom must be a string\n");
                return false;
            }
            strncpy(restraint->rec_atom_id.atom_name, json_string_value(tmp), 4);
            break;
    }

    switch (restraint->lig_type) {
        case R_RESIDUE:
            tmp = json_object_get(obj, "lig_chain");
            if (!tmp) {
                fprintf(stderr, "lig_chain for specifying residue chain must be present\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "lig_chain must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->lig_resi_id.chain = chain[0];

            tmp = json_object_get(obj, "lig_resid");
            if (!tmp) {
                tmp = json_object_get(obj, "lig_resi");
            }
            if (!tmp) {
                fprintf(stderr, "lig_resi key for specifying residue not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->lig_resi_id.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->lig_resi_id.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->lig_resi_id.residue_seq = ivalue;
            } else {
                fprintf(stderr, "lig_resi must be an integer or string\n");
                return false;
            }
            break;
        case R_CHAIN:
            tmp = json_object_get(obj, "lig_chain");
            if (!tmp) {
                fprintf(stderr, "lig_chain for specifying residue chain not present\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "lig_chain not string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->lig_chain = chain[0];
            break;
        case R_AG:
            break;
        case R_RANGE:
            atom = json_object_get(obj, "lig_atom_start");
            if (!atom) {
                fprintf(stderr, "lig_atom_start for specifying start of range must be present\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "lig_atom_start must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in lig_atom_start must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->lig_range.atom_start.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for lig_atom_start not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->lig_range.atom_start.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->lig_range.atom_start.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->lig_range.atom_start.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in lig_atom_start must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "lig_atom_start must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in lig_atom_start must be a string\n");
                return false;
            }
            strncpy(restraint->lig_range.atom_start.atom_name, json_string_value(tmp), 4);

            atom = json_object_get(obj, "lig_atom_end");
            if (!atom) {
                fprintf(stderr, "lig_atom_end for specifying end of range must be present\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "lig_atom_end must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in lig_atom_end must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->lig_range.atom_end.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for lig_atom_end not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->lig_range.atom_end.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->lig_range.atom_end.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->lig_range.atom_end.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in lig_atom_end must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "lig_atom_end must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in lig_atom_end must be a string\n");
                return false;
            }
            strncpy(restraint->lig_range.atom_end.atom_name, json_string_value(tmp), 4);
            break;
        case R_ATOM:
            atom = json_object_get(obj, "lig_atom");
            if (!atom) {
                fprintf(stderr, "lig_atom must be present for atom type restraints\n");
                return false;
            }
            tmp = json_object_get(atom, "chain");
            if (!tmp) {
                fprintf(stderr, "lig_atom must have a chain\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "chain in lig_atom must be a string\n");
                return false;
            }
            chain = json_string_value(tmp);
            restraint->lig_atom_id.chain = chain[0];

            tmp = json_object_get(atom, "resid");
            if (!tmp) {
                tmp = json_object_get(atom, "resi");
            }
            if (!tmp) {
                fprintf(stderr, "resi key for specifying residue for lig_atom not present\n");
                return false;
            }
            if (json_is_string(tmp)) {
                resind = json_string_value(tmp);
                restraint->lig_atom_id.residue_seq = atoi(resind);
                size_t end = strlen(resind);
                if (isalpha(resind[end-1])) {
                    restraint->lig_atom_id.insertion = resind[end-1];
                }
            } else if (json_is_integer(tmp)) {
                int ivalue = json_integer_value(tmp);
                restraint->lig_atom_id.residue_seq = ivalue;
            } else {
                fprintf(stderr, "resi in lig_atom must be an integer or string\n");
                return false;
            }
            tmp = json_object_get(atom, "atom_name");
            if (!tmp) {
                fprintf(stderr, "lig_atom must have an atom_name\n");
                return false;
            }
            if (!json_is_string(tmp)) {
                fprintf(stderr, "atom_name in lig_atom must be a string\n");
                return false;
            }
            strncpy(restraint->lig_atom_id.atom_name, json_string_value(tmp), 4);
            break;
    }


    return true;
}

bool restraint_set_attach_rec(struct restraint_set *rs,
                              const struct mol_atom_group *rec)
{
    if ( !(rs && rec) )
        return false;

    struct restraint *r;
    struct mol_residue *res;
    ssize_t index;

    for (size_t i = 0; i < rs->nrestraints_total; ++i)
    {
        r = rs->all_restraints[i];

        switch(r->rec_type) {
            case R_RESIDUE:
                res = find_residue(rec, r->rec_resi_id.chain,
                                   r->rec_resi_id.residue_seq,
                                   r->rec_resi_id.insertion);
                if (res == NULL) {
                    fprintf(stderr, "receptor residue %c %d%c not found\n", r->rec_resi_id.chain,
                           r->rec_resi_id.residue_seq, r->rec_resi_id.insertion);
                    return false;
                }

                r->rec_atom_start = res->atom_start;
                r->rec_atom_end = res->atom_end;
                break;
            case R_CHAIN:
                r->rec_atom_start = SIZE_MAX;
                r->rec_atom_end = 0;
                for (size_t j = 0; j < rec->natoms; j++) {
                    if (rec->residue_id[j].chain == r->rec_chain) {
                        if (j < r->rec_atom_start) {
                            r->rec_atom_start = j;
                        }
                        if (j > r->rec_atom_end) {
                               r->rec_atom_end = j;
                        }
                    }
                }
                break;
            case R_AG:
                r->rec_atom_start = 0;
                r->rec_atom_end = rec->natoms-1;
                break;
            case R_RANGE:
                index = mol_find_atom(rec, r->rec_range.atom_start.chain,
                                   r->rec_range.atom_start.residue_seq,
                                   r->rec_range.atom_start.insertion,
                                   r->rec_range.atom_start.atom_name);
                if (index == -1) {
                    fprintf(stderr, "receptor atom %c %d%c %s not found\n", r->rec_range.atom_start.chain,
                           r->rec_range.atom_start.residue_seq, r->rec_range.atom_start.insertion,
                           r->rec_range.atom_start.atom_name);
                    return false;
                }
                r->rec_atom_start = index;

                index = mol_find_atom(rec, r->rec_range.atom_end.chain,
                                   r->rec_range.atom_end.residue_seq,
                                   r->rec_range.atom_end.insertion,
                                   r->rec_range.atom_end.atom_name);
                if (index == -1) {
                    fprintf(stderr, "receptor atom %c %d%c %s not found\n", r->rec_range.atom_end.chain,
                           r->rec_range.atom_end.residue_seq, r->rec_range.atom_end.insertion,
                           r->rec_range.atom_end.atom_name);
                    return false;
                }
                r->rec_atom_end = index;
                break;
            case R_ATOM:
                index = mol_find_atom(rec, r->rec_atom_id.chain,
                                   r->rec_atom_id.residue_seq,
                                   r->rec_atom_id.insertion,
                                   r->rec_atom_id.atom_name);
                if (index == -1) {
                    fprintf(stderr, "receptor atom %c %d%c %s not found\n", r->rec_atom_id.chain,
                           r->rec_atom_id.residue_seq, r->rec_atom_id.insertion,
                           r->rec_atom_id.atom_name);
                    return false;
                }
                r->rec_atom_start = index;
                r->rec_atom_end = index;
                break;
        }
    }

    // sort all restraints
    qsort(rs->all_restraints, rs->nrestraints_total,
          sizeof(struct residue *), restraintp_cmp);

    rs->rec = rec;
//    set_rec_diameters(rs, rec);

    return true;
}

bool restraint_set_attach_lig(struct restraint_set *rs,
                              const struct mol_atom_group *lig)
{
    if ( !(rs && lig) )
        return false;

    struct restraint *r;
    struct mol_residue *res;
    ssize_t index;

    for (size_t i = 0; i < rs->nrestraints_total; ++i)
    {
        r = rs->all_restraints[i];

        switch(r->lig_type) {
            case R_RESIDUE:
                res = find_residue(lig, r->lig_resi_id.chain,
                                   r->lig_resi_id.residue_seq,
                                   r->lig_resi_id.insertion);
                if (res == NULL) {
                    fprintf(stderr, "ligand residue %c %d%c not found\n", r->lig_resi_id.chain,
                           r->lig_resi_id.residue_seq, r->lig_resi_id.insertion);
                    return false;
                }

                r->lig_atom_start = res->atom_start;
                r->lig_atom_end = res->atom_end;
                break;
            case R_CHAIN:
                r->lig_atom_start = SIZE_MAX;
                r->lig_atom_end = 0;
                for (size_t j = 0; j < lig->natoms; j++) {
                    if (lig->residue_id[j].chain == r->lig_chain) {
                        if (j < r->lig_atom_start) {
                            r->lig_atom_start = j;
                        }
                        if (j > r->lig_atom_end) {
                               r->lig_atom_end = j;
                        }
                    }
                }
                break;
            case R_AG:
                r->lig_atom_start = 0;
                r->lig_atom_end = lig->natoms-1;
                break;
            case R_RANGE:
                index = mol_find_atom(lig, r->lig_range.atom_start.chain,
                                   r->lig_range.atom_start.residue_seq,
                                   r->lig_range.atom_start.insertion,
                                   r->lig_range.atom_start.atom_name);
                if (index == -1) {
                    fprintf(stderr, "ligand atom %c %d%c %s not found\n", r->lig_range.atom_start.chain,
                           r->lig_range.atom_start.residue_seq, r->lig_range.atom_start.insertion,
                           r->lig_range.atom_start.atom_name);
                    return false;
                }
                r->lig_atom_start = index;

                index = mol_find_atom(lig, r->lig_range.atom_end.chain,
                                   r->lig_range.atom_end.residue_seq,
                                   r->lig_range.atom_end.insertion,
                                   r->lig_range.atom_end.atom_name);
                if (index == -1) {
                    fprintf(stderr, "ligand atom %c %d%c %s not found\n", r->lig_range.atom_end.chain,
                           r->lig_range.atom_end.residue_seq, r->lig_range.atom_end.insertion,
                           r->lig_range.atom_end.atom_name);
                    return false;
                }
                r->lig_atom_end = index;
                break;
            case R_ATOM:
                index = mol_find_atom(lig, r->lig_atom_id.chain,
                                   r->lig_atom_id.residue_seq,
                                   r->lig_atom_id.insertion,
                                   r->lig_atom_id.atom_name);
                if (index == -1) {
                    fprintf(stderr, "ligand atom %c %d%c %s not found\n", r->lig_atom_id.chain,
                           r->lig_atom_id.residue_seq, r->lig_atom_id.insertion,
                           r->lig_atom_id.atom_name);
                    return false;
                }
                r->lig_atom_start = index;
                r->lig_atom_end = index;
                break;
        }
    }

    rs->lig = lig;
//    set_lig_diameters(rs, lig);

    return true;
}

void set_rec_diameters(struct restraint_set *rs, const struct mol_atom_group *rec)
{
    struct restraint_group *rg;
    struct restraint *r;
    for (size_t i = 0; i < rs->ngroups; ++i)
    {
        rg = &rs->groups[i];
        for (size_t j = 0; j < rg->nrestraints; ++j)
        {
            r = &rg->restraints[j];

            r->rec_diameter = residue_diameter(rec, 0); // FIXME
        }
    }
}

void set_lig_diameters(struct restraint_set *rs, const struct mol_atom_group *lig)
{
    struct restraint_group *rg;
    struct restraint *r;
    for (size_t i = 0; i < rs->ngroups; ++i)
    {
        rg = &rs->groups[i];
        for (size_t j = 0; j < rg->nrestraints; ++j)
        {
            r = &rg->restraints[j];

            r->lig_diameter = residue_diameter(lig, 0); // FIXME
        }
    }
}

void restraint_group_destroy(struct restraint_group *rg)
{
    if (rg && rg->restraints) {
        for (size_t i = 0; i < rg->nrestraints; ++i)
        {
            free_if_not_null(rg->restraints[i].feasible_vecs);
        }
        free(rg->restraints);
    }
}

void restraint_set_destroy(struct restraint_set *rs)
{
    if (rs)
    {
        if (rs->groups)
        {
            for (size_t i = 0; i < rs->ngroups; ++i)
            {
                restraint_group_destroy(&rs->groups[i]);
            }

            free_if_not_null(rs->all_restraints);
            free(rs->groups);
        }
    }
}

bool check_restraint_set(const struct restraint_set *rs,
                         const struct mol_vector3 tv)
{
    if (!rs)
        return false;

    struct restraint_group *rg;

    size_t satisfied_in_set = 0;
    for (size_t i = 0; i < rs->ngroups; ++i) {
        // return true early if we have enough good groups
        if (satisfied_in_set >= rs->required)
            return true;

        rg = &rs->groups[i];

        if ( check_restraint_group(rg, rs->rec, rs->lig, tv) )
            satisfied_in_set += 1;

        // return false early if we have enough bad groups
        if ((i - satisfied_in_set) > (rs->ngroups - rs->required))
            return false;
    }

    return (satisfied_in_set >= rs->required);
}

bool check_restraint_group(const struct restraint_group *rg,
                           const struct mol_atom_group *rec,
                           const struct mol_atom_group *lig,
                           const struct mol_vector3 tv)
{
    struct restraint *r;

    size_t satisfied_in_group = 0;
    for (size_t i = 0; i < rg->nrestraints; ++i)
    {
        // If we have enough success, return true
        if (satisfied_in_group >= rg->required)
            return true;

        r = &rg->restraints[i];

        if ( check_restraint(r, rec, lig, tv) )
            satisfied_in_group += 1;

        // If we have too many failures, return false
        if ((i - satisfied_in_group) > (rg->nrestraints - rg->required))
            return false;
    }

    return (satisfied_in_group <= rg->required);
}

bool check_restraint(const struct restraint *r,
                     const struct mol_atom_group *rec,
                     const struct mol_atom_group *lig,
                     const struct mol_vector3 tv)
{
    struct mol_vector3 *rec_coord;
    struct mol_vector3 lig_coord;
    double dist;

    for (size_t i = r->rec_atom_start; i <= r->rec_atom_end; i++) {
        rec_coord = &rec->coords[i];
        for (size_t j = r->lig_atom_start; j <= r->lig_atom_end; j++) {
            MOL_VEC_ADD(lig_coord, lig->coords[j], tv);

            dist = MOL_VEC_EUCLIDEAN_DIST_SQ(*rec_coord, lig_coord);
            if (dist >= r->dmin_sq && dist <= r->dmax_sq) {
                return true;
            }
        }
    }

    return false;
}

void generate_feasible_regions(struct restraint_set *rs, double cell_size)
{
    rs->cell_size = cell_size;

    struct restraint *r;
    for (size_t i = 0; i < rs->nrestraints_total; ++i)
    {
        r = rs->all_restraints[i];

        generate_feasible_region(r, cell_size, rs->rec,
                                 &r->feasible_vecs, &r->nfeasible_vecs);
    }
}

void generate_feasible_region(const struct restraint *r, double cell_size,
                              const struct mol_atom_group *rec,
                              struct mol_vector3i **vectors, size_t *nvectors)
{
    size_t max_vectors = 64; // Initial size
    *vectors = calloc(max_vectors, sizeof(struct mol_vector3));
    *nvectors = 0;

    struct mol_vector3 coord;
    struct mol_vector3i grid_coord;
    struct mol_vector3i tv;
    struct mol_vector3i min, max;
    double dist_sq;
    MOL_VEC_DIV_SCALAR(min, rec->coords[r->rec_atom_start], cell_size);
    MOL_VEC_DIV_SCALAR(max, rec->coords[r->rec_atom_start], cell_size);
    MOL_VEC_ADD_SCALAR(max, max, 1);

    for (size_t i = r->rec_atom_start + 1; i <= r->rec_atom_end; i++) {
        grid_coord.X = (int) (rec->coords[i].X / cell_size);
        grid_coord.Y = (int) (rec->coords[i].Y / cell_size);
        grid_coord.Z = (int) (rec->coords[i].Z / cell_size);

        min.X = MIN(min.X, grid_coord.X);
        min.Y = MIN(min.Y, grid_coord.Y);
        min.Z = MIN(min.Z, grid_coord.Z);
        max.X = MAX(max.X, grid_coord.X + 1);
        max.Y = MAX(max.Y, grid_coord.Y + 1);
        max.Z = MAX(max.Z, grid_coord.Z + 1);
    }

    MOL_VEC_SUB_SCALAR(min, min, r->dmax/cell_size);
    MOL_VEC_ADD_SCALAR(max, max, r->dmax/cell_size);

    for (tv.X = min.X; tv.X <= max.X; tv.X++) {
        for (tv.Y = min.Y; tv.Y <= max.Y; tv.Y++) {
            for (tv.Z = min.Z; tv.Z <= max.Z; tv.Z++) {
                MOL_VEC_MULT_SCALAR(coord, tv, cell_size);

                for (size_t i = r->rec_atom_start; i <= r->rec_atom_end; i++) {
                    dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(rec->coords[i], coord);

                    if (dist_sq >= r->dmin_sq && dist_sq <= r->dmax_sq) {
                        if (*nvectors == max_vectors) {
                            max_vectors *= 2;
                            *vectors = realloc(*vectors, max_vectors * sizeof(struct mol_vector3));
                        }

                        (*vectors)[*nvectors] = tv;
                        (*nvectors) += 1;
                        break;
                    }
                }
            }
        }
    }

    // Repack vectors
    *vectors = realloc(*vectors, (*nvectors) * sizeof(struct mol_vector3));
}

bool restraint_set_satisfiable(const struct restraint_set *rs,
                               const struct mol_atom_group *lig,
                               const struct mol_vector3i offset,
                               const struct mol_vector3i grid_size,
                               struct mol_v3hash *tvh)
{
    if (lig == NULL) {
        lig = rs->lig;
    }

    struct mol_vector3i min_limit;
    struct mol_vector3i max_limit;
    MOL_VEC_MULT_SCALAR(min_limit, offset, -1);
    MOL_VEC_SUB(max_limit, grid_size, offset);

    struct restraint_group *rg;
    struct restraint *r;
    mol_tvhash set_hash, group_hash, restraint_hash, group_required_hash;
    mol_tvhash_create(&set_hash);
    mol_tvhash_create(&group_hash);
    mol_tvhash_create(&restraint_hash);
    mol_tvhash_create(&group_required_hash);

    struct mol_vector3i translation;
    struct mol_vector3i key;
    size_t val;
    struct mol_vector3 tvec;
    size_t strict_group_required = 0;
    size_t val_group_required;

    for (size_t i = 0; i < rs->ngroups; ++i) {
        rg = &rs->groups[i];
        if (rg->group_required) {
            strict_group_required += 1;
        }

        for (size_t j = 0; j < rg->nrestraints; ++j) {
            r = &rg->restraints[j];

            for (size_t k = r->lig_atom_start; k <= r->lig_atom_end; ++k) {
                for (size_t veci = 0; veci < r->nfeasible_vecs; ++veci) {
                    MOL_VEC_DIV_SCALAR(tvec, lig->coords[k], rs->cell_size);
                    MOL_VEC_SUB(tvec, tvec, r->feasible_vecs[veci]);

                    translation.X = (int) round(tvec.X);
                    translation.Y = (int) round(tvec.Y);
                    translation.Z = (int) round(tvec.Z);

                    if (translation.X < min_limit.X ||
                        translation.Y < min_limit.Y ||
                        translation.Z < min_limit.Z ||
                        translation.X >= max_limit.X ||
                        translation.Y >= max_limit.Y ||
                        translation.Z >= max_limit.Z) {
                        continue;
                    }

                    mol_tvhash_set(&restraint_hash, translation, 1);
                }
            }

            kh_foreach(
                restraint_hash.h, key, val,
                mol_tvhash_incr(&group_hash, key););
            mol_tvhash_reset(&restraint_hash);
        }

        kh_foreach(
            group_hash.h, key, val,
            assert(val <= rg->nrestraints);
            if (val >= rg->required) {
                mol_tvhash_incr(&set_hash, key);
                if (rg->group_required) {
                    mol_tvhash_incr(&group_required_hash, key);
                }
            }
            );
            mol_tvhash_reset(&group_hash);
    }

    kh_foreach(
        set_hash.h, key, val,
        assert(val <= rs->ngroups);
        if (val >= rs->required) {
            val_group_required = mol_tvhash_get(&group_required_hash, key);
            if (val_group_required >= strict_group_required) {
                MOL_VEC_ADD(key, key, offset);
                mol_tvhash_set(tvh, key, val);
            }
        });

    mol_tvhash_destroy(&set_hash);
    mol_tvhash_destroy(&group_hash);
    mol_tvhash_destroy(&restraint_hash);
    mol_tvhash_destroy(&group_required_hash);

    return mol_tvhash_size(tvh) > 0;
}

// util functions
double residue_diameter(const struct mol_atom_group *ag, size_t resi)
{
    if (resi >= kh_size(ag->residues))
    {
        fprintf(stderr, "Residue index out of bounds: %zd\n", resi);
        return 0.0;
    }

    int start = 0; // TODO: fix to use residue hash
    int end = 0;

    double diameter = 0.0;
    double dist;

    struct mol_vector3 delta;
    for (int i = start; i < end; ++i)
    {
        for (int j = i + 1; j < end; ++j)
        {
            MOL_VEC_SUB(delta, ag->coords[i], ag->coords[j]);
            dist = MOL_VEC_SQ_NORM(delta);

            diameter = fmax(diameter, dist);
        }
    }
    diameter = sqrt(diameter);

    return diameter;
}

// Output functions
void print_restraint_set(struct restraint_set *rs)
{
    fprint_restraint_set(stdout, rs);
}

void fprint_restraint_set(FILE *fp, struct restraint_set *rs)
{
    if (fp == NULL || rs == NULL || rs->groups == NULL)
        return;

    struct restraint_group *rg;
    for (size_t i = 0; i < rs->ngroups; ++i)
    {
        rg = &rs->groups[i];
        for (size_t j = 0; j < rg->nrestraints; ++j)
        {
            struct restraint *r = &rg->restraints[j];
            fprintf(fp, "rec(%zd, %zd) to lig(%zd, %zd) (%f %f %f)\n",
                    r->rec_atom_start, r->rec_atom_end,
                    r->lig_atom_start, r->lig_atom_end,
                    r->dmin, r->dist, r->dmax);
        }
    }
}

int restraintp_cmp(const void *a, const void *b)
{
    const struct restraint *ra = *((struct restraint * const*) a);
    const struct restraint *rb = *((struct restraint * const*) b);

    if (ra == rb)
        return 0;

    if (ra->rec_atom_start - rb->rec_atom_start)
        return ((int) ra->rec_atom_start - (int) rb->rec_atom_start);

    if (ra->rec_atom_end - rb->rec_atom_end)
        return ((int) ra->rec_atom_end - (int) rb->rec_atom_end);

    if (ra->lig_atom_start - rb->lig_atom_start)
        return ((int) ra->lig_atom_start - (int) rb->lig_atom_start);

    if (ra->lig_atom_end - rb->lig_atom_end)
        return ((int) ra->lig_atom_end - (int) rb->lig_atom_end);

    return 0;
}
