#ifndef _THASH_H_
#define _THASH_H_

#include "mol2/vector.h"

#include "khash.h"

#include <assert.h>

__KHASH_TYPE(TVH, struct mol_vector3i, size_t)

struct mol_v3hash
{
  khash_t(TVH) *h;
};

typedef struct mol_v3hash mol_tvhash;

void mol_tvhash_create(mol_tvhash *tvh);
void mol_tvhash_reset(mol_tvhash *tvh);
void mol_tvhash_destroy(mol_tvhash *tvh);

size_t mol_tvhash_size(const mol_tvhash *tvh);
size_t mol_tvhash_get(const mol_tvhash *tvh,
                      const struct mol_vector3i k);
void mol_tvhash_set(mol_tvhash *tvh,
                    const struct mol_vector3i k, size_t v);
void mol_tvhash_incr(mol_tvhash *tvh,
                     const struct mol_vector3i k);

#endif /* _THASH_H_ */
