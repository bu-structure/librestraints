#pragma once

#include "tvhash.h"

#include "mol2/atom_group.h"
#include "mol2/vector.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

enum restraint_type {
    R_RESIDUE,
    R_CHAIN,
    R_AG, //whole receptor or ligand
    R_RANGE,
    R_ATOM,
};

struct restraint_atom_id {
    int16_t residue_seq;
    char chain;
    char insertion;
    char atom_name[5];
};

struct restraint_atom_range {
    struct restraint_atom_id atom_start;
    struct restraint_atom_id atom_end;
};

struct restraint
{
    size_t rec_atom_start, rec_atom_end;
    size_t lig_atom_start, lig_atom_end;

    enum restraint_type rec_type;
    enum restraint_type lig_type;

    union {
        struct mol_residue_id rec_resi_id;
        char rec_chain;
        struct restraint_atom_range rec_range;
        struct restraint_atom_id rec_atom_id;
    };
    union {
        struct mol_residue_id lig_resi_id;
        char lig_chain;
        struct restraint_atom_range lig_range;
        struct restraint_atom_id lig_atom_id;
    };

    double rec_diameter, lig_diameter;

    double dmin, dmax;
    double dist;

    double dmin_sq, dmax_sq;

    size_t nfeasible_vecs;
    struct mol_vector3i *feasible_vecs;
};

struct restraint_group
{
    size_t nrestraints;
    struct restraint *restraints;

    size_t required; // number of restraints which must be satisfied
    bool group_required;
};

struct restraint_set
{
    size_t ngroups;
    struct restraint_group *groups;

    size_t required; // number of groups which must be satisfied

    const struct mol_atom_group *rec, *lig;

    double cell_size;

    // "private" variables
    size_t nrestraints_total;
    struct restraint **all_restraints;
};

bool restraint_set_read_json(struct restraint_set *rs, const char *restraints_path);
bool restraint_set_fread_json(struct restraint_set *rs, FILE *fp);

bool restraint_set_attach_rec(struct restraint_set *rs,
                              const struct mol_atom_group *rec);
bool restraint_set_attach_lig(struct restraint_set *rs,
                              const struct mol_atom_group *lig);
void set_rec_diameters(struct restraint_set *rs, const struct mol_atom_group *rec);
void set_lig_diameters(struct restraint_set *rs, const struct mol_atom_group *lig);

void restraint_set_destroy(struct restraint_set *rs);

bool check_restraint_set(const struct restraint_set *rs,
                         const struct mol_vector3 tv);
bool check_restraint_group(const struct restraint_group *rg,
                           const struct mol_atom_group *rec,
                           const struct mol_atom_group *lig,
                           const struct mol_vector3 tv);
bool check_restraint(const struct restraint *r,
                     const struct mol_atom_group *rec,
                     const struct mol_atom_group *lig,
                     const struct mol_vector3 tv);

void generate_feasible_regions(struct restraint_set *rs,  double cell_size);
void generate_feasible_region(const struct restraint *r, double cell_size,
                              const struct mol_atom_group *rec,
                              struct mol_vector3i **vectors, size_t *nvectors);

/**
 * Checks restraint set is satisfiable. If true, tvh is filled translation
 * indexes for the given grid size
 */
bool restraint_set_satisfiable(const struct restraint_set *rs,
                               const struct mol_atom_group *lig,
                               const struct mol_vector3i offset,
                               const struct mol_vector3i grid_size,
                               struct mol_v3hash *tvh);

/**
 * Generate sets of translations which satisfy a restraint, given
 * rec and lig. Expects tvh to be initialized and empty
 */
// void restraint_tvhash_set

// comparators for pointers
int restraintp_cmp(const void *a, const void *b);

/**
 * Returns the maximum distance between any two atoms in a given residue
 */
double residue_diameter(const struct mol_atom_group *ag, size_t resi);

/**
 * Prints the restraints to stdout/file. TODO: define format
 */
void print_restraint_set(struct restraint_set *rs);
void fprint_restraint_set(FILE *fp, struct restraint_set *rs);
