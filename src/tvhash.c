#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include "tvhash.h"

#include <stdio.h>
#include <stdlib.h>

static khint_t __mol_vector3i_hash_func(const struct mol_vector3i key)
{
    return (key.X << 20) + (key.Y << 10) + key.Z;
}

static khint_t __mol_vector3i_hash_equal(const struct mol_vector3i a,
                                        const struct mol_vector3i b)
{
    return (a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z);
}

__KHASH_IMPL(TVH, static kh_inline, struct mol_vector3i, size_t, 1,
             __mol_vector3i_hash_func, __mol_vector3i_hash_equal)

void mol_tvhash_create(mol_tvhash *tvh)
{
    tvh->h = kh_init(TVH);
}

void mol_tvhash_reset(mol_tvhash *tvh)
{
    kh_clear(TVH, tvh->h);
}

void mol_tvhash_destroy(mol_tvhash *tvh)
{
    kh_destroy(TVH, tvh->h);
}


size_t mol_tvhash_size(const mol_tvhash *tvh)
{
    return kh_size(tvh->h);
}

size_t mol_tvhash_get(const mol_tvhash *tvh,
                      const struct mol_vector3i k)
{
    khiter_t key = kh_get(TVH, tvh->h, k);
    if (key != kh_end(tvh->h))
        return kh_value(tvh->h, key);
    else
        return 0;
}

void mol_tvhash_set(mol_tvhash *tvh,
                    const struct mol_vector3i k, size_t v)
{
    int ret;
    khiter_t key = kh_put(TVH, tvh->h, k, &ret);

    kh_value(tvh->h, key) = v;
}

void mol_tvhash_incr(mol_tvhash *tvh,
                     const struct mol_vector3i k)
{
    int ret;
    khiter_t key = kh_put(TVH, tvh->h, k, &ret);

    if (!ret)
        kh_value(tvh->h, key) = kh_value(tvh->h, key) + 1;
    else
        kh_value(tvh->h, key) = 1;
}
