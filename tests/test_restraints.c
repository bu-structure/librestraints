#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <stdbool.h>
#include <math.h>

#include "mol2/atom_group.h"
#include "mol2/pdb.h"

#include "restraints.h"

START_TEST(test_reading)
{
    struct restraint_set rs;
    memset(&rs, 0, sizeof(struct restraint_set));

    restraint_set_read_json(&rs, "data/test_restraints.json");
    ck_assert_uint_eq(rs.ngroups, 1);
    ck_assert_uint_eq(rs.groups[0].nrestraints, 5);
    ck_assert_uint_eq(rs.nrestraints_total, 5);

    restraint_set_destroy(&rs);
}
END_TEST

START_TEST(test_reading2)
{
    struct restraint_set rs;
    memset(&rs, 0, sizeof(struct restraint_set));

    restraint_set_read_json(&rs, "data/test_restraints3.json");
    ck_assert_uint_eq(rs.ngroups, 2);
    ck_assert_uint_eq(rs.groups[0].nrestraints, 5);
    ck_assert_uint_eq(rs.groups[1].nrestraints, 5);
    ck_assert(rs.groups[0].group_required == true);
    ck_assert(rs.groups[1].group_required == false);
    ck_assert_uint_eq(rs.nrestraints_total, 10);

    restraint_set_destroy(&rs);
}
END_TEST

START_TEST(test_attach_reclig)
{
    struct restraint_set rs;
    memset(&rs, 0, sizeof(struct restraint_set));

    restraint_set_read_json(&rs, "data/test_restraints.json");
    struct mol_atom_group *rec = mol_read_pdb("data/rec.pdb");
    struct mol_atom_group *lig = mol_read_pdb("data/lig.pdb");

    ck_assert(restraint_set_attach_rec(&rs, rec));
    ck_assert(restraint_set_attach_lig(&rs, lig));

    mol_atom_group_free(rec);
    mol_atom_group_free(lig);
    restraint_set_destroy(&rs);
}
END_TEST

START_TEST(test_attach_reclig2)
{
    struct restraint_set rs;
    memset(&rs, 0, sizeof(struct restraint_set));

    restraint_set_read_json(&rs, "data/test_restraints2.json");
    struct mol_atom_group *rec = mol_read_pdb("data/rec2.pdb");
    struct mol_atom_group *lig = mol_read_pdb("data/lig2.pdb");

    ck_assert(restraint_set_attach_rec(&rs, rec));
    ck_assert(restraint_set_attach_lig(&rs, lig));

    //both residue
    ck_assert_uint_eq(rs.groups[0].restraints[0].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[0].rec_atom_end, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[0].lig_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[0].lig_atom_end, 0);

    //rec chain, lig residue
    ck_assert_uint_eq(rs.groups[0].restraints[1].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[1].rec_atom_end, 4);
    ck_assert_uint_eq(rs.groups[0].restraints[1].lig_atom_start, 1);
    ck_assert_uint_eq(rs.groups[0].restraints[1].lig_atom_end, 1);

    //rec residue, lig chain
    ck_assert_uint_eq(rs.groups[0].restraints[2].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[2].rec_atom_end, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[2].lig_atom_start, 5);
    ck_assert_uint_eq(rs.groups[0].restraints[2].lig_atom_end, 9);

    //rec all, lig residue
    ck_assert_uint_eq(rs.groups[0].restraints[3].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[3].rec_atom_end, 9);
    ck_assert_uint_eq(rs.groups[0].restraints[3].lig_atom_start, 3);
    ck_assert_uint_eq(rs.groups[0].restraints[3].lig_atom_end, 3);

    //rec residue, lig all
    ck_assert_uint_eq(rs.groups[0].restraints[4].rec_atom_start, 4);
    ck_assert_uint_eq(rs.groups[0].restraints[4].rec_atom_end, 4);
    ck_assert_uint_eq(rs.groups[0].restraints[4].lig_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[4].lig_atom_end, 9);

    //rec range, lig residue
    ck_assert_uint_eq(rs.groups[0].restraints[5].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[5].rec_atom_end, 4);
    ck_assert_uint_eq(rs.groups[0].restraints[5].lig_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[5].lig_atom_end, 0);

    //rec residue, lig range
    ck_assert_uint_eq(rs.groups[0].restraints[6].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[6].rec_atom_end, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[6].lig_atom_start, 5);
    ck_assert_uint_eq(rs.groups[0].restraints[6].lig_atom_end, 9);

    //rec atom, lig residue
    ck_assert_uint_eq(rs.groups[0].restraints[7].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[7].rec_atom_end, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[7].lig_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[7].lig_atom_end, 0);

    //rec atom, lig range
    ck_assert_uint_eq(rs.groups[0].restraints[8].rec_atom_start, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[8].rec_atom_end, 0);
    ck_assert_uint_eq(rs.groups[0].restraints[8].lig_atom_start, 5);
    ck_assert_uint_eq(rs.groups[0].restraints[8].lig_atom_end, 5);

    mol_atom_group_free(rec);
    mol_atom_group_free(lig);
    restraint_set_destroy(&rs);
}
END_TEST

START_TEST(test_generate_feasible)
{
    struct restraint_set rs;
    memset(&rs, 0, sizeof(struct restraint_set));

    restraint_set_read_json(&rs, "data/test_restraints.json");
    struct mol_atom_group *rec = mol_read_pdb("data/rec.pdb");
    struct mol_atom_group *lig = mol_read_pdb("data/lig.pdb");

    ck_assert(restraint_set_attach_rec(&rs, rec));
    ck_assert(restraint_set_attach_lig(&rs, lig));

    generate_feasible_regions(&rs, 1.0);
    for(size_t i = 0; i < rs.nrestraints_total; i++) {
        ck_assert_uint_eq(rs.all_restraints[i]->nfeasible_vecs, 832);
    }

    struct mol_v3hash tvh;
    mol_tvhash_create(&tvh);
    ck_assert(restraint_set_satisfiable(&rs, NULL, (struct mol_vector3i) {500,500,500},
                                       (struct mol_vector3i) {1000,1000,1000}, &tvh));
    ck_assert_uint_eq(mol_tvhash_size(&tvh), 1063);

    mol_tvhash_destroy(&tvh);
    mol_atom_group_free(rec);
    mol_atom_group_free(lig);
    restraint_set_destroy(&rs);
}
END_TEST

Suite *restraint_suite(void)
{
    Suite *suite = suite_create("restraints");

    TCase *tcase_simple = tcase_create("test_simple");
    tcase_add_test(tcase_simple, test_reading);
    tcase_add_test(tcase_simple, test_reading2);
    tcase_add_test(tcase_simple, test_attach_reclig);
    tcase_add_test(tcase_simple, test_attach_reclig2);
    tcase_add_test(tcase_simple, test_generate_feasible);

    suite_add_tcase(suite, tcase_simple);

    return suite;
}

int main(void)
{
    Suite *suite = restraint_suite();
    SRunner *runner = srunner_create(suite);
    srunner_run_all(runner, CK_ENV);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    return number_failed;
}
