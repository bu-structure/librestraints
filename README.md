librestraints
=============

Spatial restraints library.

This allows restraint set to be specified using a json format. These restraints
are used in the piper protein-protein docking program, but there are feasibly
other uses.

This library depends on libmol2

## Definition of a restraint set
A restraint set is made up of groups of restraints. The library allows you to
specify how many groups must be satisfied, and then how many restraints within a
group must be specified. This two-level specification provides a lot of flexibility.

At the highest level, a restraint set is a dictionary with two keys: `groups`, which
has as a value an array of groups of restraints; and `required`, which is an integer
setting the number of groups that have to be satisfied.

### Definition of a restraint group
Each restraint group is a dictionary with two keys (sensing a theme?)):
`restraints`, which is an array of restraints; and `required`, which is an integer
setting the number of restraints that have to be satisfied for the group to be
satisfied.

A restraint group can also have the key `group_required` with the value `true` to
indicate that group is always required to be satisfied. This group will still count
towards the minimum number of groups that must be satisfied. As an example of use,
if you have four groups, one of which you want always satisfied and you want at
least two of the other three satisfied, you would set `group_required` to `true`
on the first group and specify overall that at least 3 groups are `required`.
If the number of groups with `group_required` specified as `true` are greater than
the number specified as `required`, then `required` will effectively have no impact
on the results.


### Definition of a restraint
A restraint is between part of a receptor and a part of a ligand. Every restraint
has 4 required keys:

* floating point values`dmin` and `dmax` which means some pair of atoms in the
defined parts of the receptor and ligand must be between those distances apart
* strings `rec_type` and `lig_type` which indicate how the parts of the receptor and
ligand are defined

There are 5 possible values for `rec_type` and `ligand_type`
* residue: a single residue in the protein
* chain: a single chain in the protein
* all: the whole protein
* range: a range of atoms in the protein
* atom: a single atom in the protein

#### Residue type
If the type is residue, the restraint must have `chain` and `resi` keys prefixed
with the respective sides (i.e. `rec_chain`, `rec_resi`, `lig_chain`, `lig_resi`).

The value for the `chain` key is a one letter string specifying the receptor chain.

The value for the `resi` key is a string specifying the residue number and any
insertion code. The `resi` key can also be an integer, in which case it is assumed
there is no insertion code.

### Chain type
The chain type is similar to the residue type, but no `resi` key is specified. That
is, only a `rec_chain` or `lig_chain` should be specified.

### All type
The all type doesn't require any additional information about the protein to be
specified. It is used if you want to say that a certain atom on one side of the
complex must be close to any part of the other protein.

### Range and Atom types
The range and atom types rely on the concept of an atom, which has three keys,
`chain` and `resi` as already specified, and `atom_name`, which is the name of the
atom.

The atom type requires an atom as the value for either `rec_atom` or `lig_atom`.

The range type requires two atoms specifying the start and end of the range as
(`rec_atom_start` and `rec_atom_end`) or (`lig_atom_start` and `lig_atom_end`). Any
atoms between the start and end will be included in the range.
