#!/usr/bin/env python
from __future__ import print_function
import re
import sys

from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

AIR_PATTERN = r"""
\(\s*resid\s+(?P<s1_resi>\S+)\s+ and \s+ segid \s+ (?P<s1_seg>\S+)\s*\)  # the first residue selection
\s+\(\s*(?P<s2_sele>.+)\s*\)  # the second set of residue selections
\s+(?P<dist>[0-9]*\.[0-9]+|[0-9]+)  # dist
\s+(?P<dminus>[0-9]*\.[0-9]+|[0-9]+)  # dminus
\s+(?P<dplus>[0-9]*\.[0-9]+|[0-9]+)  # dplus
"""
AIR_REGEX = re.compile(AIR_PATTERN, re.VERBOSE | re.I)

RSEL_PATTERN = r"""\(\s*resid\s+(\S+)\s+and\s+segid\s+(\S+)\s*\)"""
RSEL_REGEX = re.compile(RSEL_PATTERN, re.I)


def parse_airs(data,
               dminus_adjust=None, dplus_adjust=None,
               rec_chain=None, lig_chain=None):
    restraint_set = {
        'groups': []
    }

    for air in data.split("assign"):
        air = air.strip().replace("\n", "")
        if not air:
            continue
        group = {
            'restraints': []
        }

        m = AIR_REGEX.match(air)
        if not m:
            print("Warning: Could not parse AIR: {}".format(air), file=sys.stderr)
            continue
        
        d = m.groupdict()
        s1_seg = d['s1_seg']
        s1_resi = d['s1_resi']
        dist = float(d['dist'])
        dminus = float(d['dminus'])
        dplus = float(d['dplus'])

        if dminus_adjust:
            dminus += dminus_adjust
        if dplus_adjust:
            dplus += dplus_adjust
        
        s2_sele = d['s2_sele'].strip()
        
        for s2_resi, s2_seg in RSEL_REGEX.findall(s2_sele):
            if s1_seg == 'A':
                rec_seg = rec_chain or s1_seg
                rec_resi = s1_resi
                lig_seg = lig_chain or s2_seg
                lig_resi = s2_resi
            else:
                rec_seg = rec_chain or s2_seg
                rec_resi = s2_resi
                lig_seg = lig_chain or s1_seg
                lig_resi = s1_resi

            restraint = {
                'type': 'residue',
                'rec_segid': rec_seg,
                'rec_resid': rec_resi,
                'lig_segid': lig_seg,
                'lig_resid': lig_resi,
                'dist': dist,
                'dminus': dminus,
                'dplus': dplus
            }
            
            group['restraints'].append(restraint)
        group['group_limit'] = len(group['restraints']) - 1
        restraint_set['groups'].append(group)
    
    return restraint_set

if __name__ == "__main__":
    import json
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("air_file", help="AIR restraints file")
    parser.add_argument("--pretty", default=False, action="store_true",
                        help="Pretty print the json")
    parser.add_argument("--dminus", default=None, type=float,
                        help="Additional distance to add to all dminus values")
    parser.add_argument("--dplus", default=None, type=float,
                        help="Additional distance to add to all dplus values")
    parser.add_argument("--rec-chain", default=None,
                        help="Use given chain for receptor")
    parser.add_argument("--lig-chain", default=None,
                        help="Use given chain for ligand")

    args = parser.parse_args()

    if args.air_file != "-":
        with open(args.air_file, 'r') as inp:
            lines = filter(lambda l: not l.strip().startswith('!') and l.strip(),
                           inp.readlines())
    else:
        lines = filter(lambda l: not l.strip().startswith('!') and l.strip(),
                       sys.stdin)

    restraints = parse_airs(''.join(lines),
                            args.dminus, args.dplus,
                            args.rec_chain, args.lig_chain)

    indent = None
    if args.pretty:
        indent = 2
    print(json.dumps(restraints, indent=indent))
